# Diseñar el código en Python, correspondiente a un programa que
# escribe el porcentaje descontado en una compra, introduciendo por teclado el
# porcentaje descontado y el precio pagado.

precio = float(input("Ingrese precio: "))
porcentaje = float(input("Ingrese descuento en %: "))

descuento = precio * (porcentaje / 100)
print("Monto descontado: Bs " + str(descuento))