# Diseñar el código en Python, correspondiente a un programa que
# pida el total de kilómetros recorridos, el precio de la gasolina (por litro) y el tiempo
# que se ha tardado (en horas y minutos) y que calcule:
# • Consumo de gasolina (en litros y Bs.) en el recorrido.
# • Velocidad media (en km/h y m/s).
# Este vehículo ofrece un rendimiento promedio de 16 kilómetros por litro.
usuario = ["Km recorridos", "precio de gasolina (por litros)", "tiempo en llegar a destino(horas)","tiempo en llegar a destino (minutos)"]
valores=[]
for i in usuario:
    valores.append(float(input("Ingrese "+ i +": ")))

horastotal = valores[2] + (valores[3]/60)
rendimiento = valores[0] / 16
bs = valores[1] * rendimiento
mediakm = valores[0] / horastotal
mediamin = mediakm / 3.6

print("Consumo de " + str(rendimiento) + " litros de gasolina")
print("Consumo de Bs "+ str(bs) + " en gasolina")
print("Velocidad media " + str(mediakm) + " km/h")
print("Velocidad media " + str(mediamin) + " m/s")