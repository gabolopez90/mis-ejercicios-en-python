# Diseñar el código en Python, correspondiente a un programa que
# calcule el área y el perímetro de un triángulo rectángulo dada la base y la altura.
import math

altura = float(input("Ingrese la altura: "))
base = float(input("Ingrese la base: "))

area  = (base * altura) / 2
perimetro = base + altura + math.sqrt(base**2+altura**2)
print("Area: " + str(area))
print("Perimetro: " + str(perimetro))